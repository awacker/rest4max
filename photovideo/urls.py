from django.conf.urls import patterns, url

from photovideo.views import *

urlpatterns = patterns('',
    url(r'^photovideo_album_up_rank', photovideo_album_up_rank, name='photovideo_album_up_rank'),
    url(r'^photovideo_album_down_rank', photovideo_album_down_rank, name='photovideo_album_down_rank'),
    url(r'^photovideo_album_name', photovideo_album_name, name='photovideo_album_name'),
    url(r'^photovideo_up_rank', photovideo_up_rank, name='photovideo_up_rank'),
    url(r'^photovideo_down_rank', photovideo_down_rank, name='photovideo_down_rank'),
    
)

