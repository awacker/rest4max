;(function($){ $(document).ready(function(){

	var r_list = $('td:contains("action")').text().split(';');
	var i;
	
	for (i = 0; i < r_list.length - 1; i++) {
		
		var extracted_id = r_list[i].split('!:')[1]
		var extracted_rank = r_list[i].split('!:')[2]
		var extracted_music_album_value = r_list[i].split('!:')[3]
		var l = '<td class="action-tool">'+

			'<a id="up_'+extracted_id+'_'+extracted_rank+'" href="#" class="link1">Rank UP</a>'+
			'<a id="down_'+extracted_id+'_'+extracted_rank+'" href="#" class="link2">Rank DOWN</a>'+
			'<a href="/admin/photovideo/photovideo/?photovideo_album_value='+extracted_id+'" class="link">Go to PhotoVideo list</a>'+
			'</td>';
		$('td:contains("'+r_list[i]+'")').replaceWith(l);
		
	}
	$('.link1').bind('click', function(event){		
		var id = event.target.id.split('_')[1];
		var rank = event.target.id.split('_')[2];

	 	$.ajax({
	        url: "/photovideo/photovideo_album_up_rank",
	        global: false,
	        type: 'POST',
	        dataType: "json",
	        data: {id: id, rank: rank},
	        success: function(data) { 
	        	window.location.reload();
	        } 
	 	});
	});
	
	$('.link2').bind('click', function(event){
		var id = event.target.id.split('_')[1];
		var rank = event.target.id.split('_')[2];

	 	$.ajax({
	        url: "/photovideo/photovideo_album_down_rank",
	        global: false,
	        type: 'POST',
	        dataType: "json",
	        data: {id: id, rank: rank},
	        success: function(data) { 
	        	window.location.reload();
	        } 
	 	});
	});


  });
})(django.jQuery);



//http://127.0.0.1:8000/admin/music/musicalbum/2/?_changelist_filters=o%3D1