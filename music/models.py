from django.db import models
from django.conf import settings
import os
from django.dispatch import receiver
import shutil
from ckeditor.fields import RichTextField
import storages.backends.s3boto
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.db.models import Max
from django.db.models import F

musuc_album_path_pref = 'music/'

def MusicAlbumFile(instance, filename):

    r_id = instance.title.replace(' ', '_').replace('/', '-')
    url = "%smusic_album_photos/%s/%s" % (musuc_album_path_pref, r_id, filename)
    return url

class MusicAlbum(models.Model):

    title = models.CharField(max_length=200, unique=True)
    photo1 = models.FileField(upload_to=MusicAlbumFile, blank=True, null=True)
    photo2 = models.FileField(upload_to=MusicAlbumFile, blank=True, null=True)
    photo3 = models.FileField(upload_to=MusicAlbumFile, blank=True, null=True)    
#    discription = models.TextField(blank=True, null=True)
    discription = RichTextField(blank=True, null=True)
    rank_value = models.IntegerField(blank=True, null=True)
              
    def _rank_value(self):
        return self.rank_value
    rank = property(_rank_value)
    
    def _name(self):
        return self.title
    name = property(_name)
    
    def _action(self):
        return 'action!:%s!:%s!:%s;' % (self.id, self.rank_value, self.title)
    action = property(_action)
    
    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):        
        if self.id == None:
            v = MusicAlbum.objects.all().aggregate(Max('rank_value'))['rank_value__max']
            if v == None:
                v = '0'
            self.rank_value = int(v) + 1
        super(MusicAlbum, self).save(*args, **kwargs)
        
        
   

def SongFile(instance, filename):

    r_id = instance.music_album_value.id
    url = "%s%s/%s" % (musuc_album_path_pref, r_id, filename)
    return url

class Song(models.Model):

    name_value = models.CharField(max_length=200)
    music_album_value = models.ForeignKey(MusicAlbum)
    file_name = models.FileField(upload_to=SongFile)
    lyrics = models.TextField(blank=True, null=True)
    rank_value = models.IntegerField(blank=True, null=True)
    def __unicode__(self):
        return self.music_album_value.title + " | " + self.name_value
    
    def _name(self):
        return self.name_value
    name = property(_name)

    def _music_album_value(self):
        return self.music_album_value.title
    music_album = property(_music_album_value)
    
    def _rank_value(self):
        return self.rank_value
    rank = property(_rank_value)
    
    def _action(self):
        return 'action!:%s!:%s!:%s;' % (self.id, self.rank_value, self.music_album_value.id)
    action = property(_action)

    def save(self, *args, **kwargs):        
        if self.id == None:
            v = Song.objects.all().filter(music_album_value=self.music_album_value.id).aggregate(Max('rank_value'))['rank_value__max']
            if v == None:
                v = '0'
                
            print v, self.music_album_value.id
            self.rank_value = int(v) + 1
            
       #     self.music_album_value = self.
        super(Song, self).save(*args, **kwargs)
        
        
# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=MusicAlbum)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    MusicAlbum.objects.filter(rank_value__gt=instance.rank_value).update(rank_value=F('rank_value') - 1)
    try:
        default_storage.delete(str(instance.photo1))
        default_storage.delete(str(instance.photo2))
        default_storage.delete(str(instance.photo3))
    except:
        pass     

    
@receiver(models.signals.post_delete, sender=Song)
def auto_delete_song_file_on_delete(sender, instance, **kwargs):
    Song.objects.filter(rank_value__gt=instance.rank_value, music_album_value=instance.music_album_value).update(rank_value=F('rank_value') - 1)
    try:
        default_storage.delete(str(instance.file_name))
    except:
        pass     

