from django.conf.urls import patterns, url

from music.views import *

urlpatterns = patterns('',
    url(r'^music_album_up_rank', music_album_up_rank, name='music_album_up_rank'),
    url(r'^music_album_down_rank', music_album_down_rank, name='music_album_down_rank'),
    url(r'^music_album_name', music_album_name, name='music_album_name'),
    url(r'^song_up_rank', song_up_rank, name='song_up_rank'),
    url(r'^song_down_rank', song_down_rank, name='song_down_rank'),
    
)

