from django.contrib import admin
from django import forms
from .models import *
from ckeditor.widgets import CKEditorWidget
from django.contrib.admin.views.main import ChangeList
from django.conf.urls import patterns


class MusicAlbumForm(forms.ModelForm):
    discription = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = MusicAlbum

class SongForm(forms.ModelForm):

    name_value = forms.CharField(label="Name", max_length=200, required=True)
    lyrics = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Song
        fields = [
            'name_value',
            'file_name',
            'lyrics',
            'rank_value',
            'music_album_value',
            ]

    
class MusicAlbumAdmin(admin.ModelAdmin):
    form = MusicAlbumForm    
    readonly_fields = ('rank_value',)
    list_display = ('name', 'rank', 'action',)
    ordering = ['rank_value']

    class Media:
        js = ['js/music_extention.js']


class SongAdmin(admin.ModelAdmin):
    form = SongForm
    ordering = ['rank_value']
    readonly_fields = ('rank_value',)
    list_display = ('name', 'music_album', 'rank', 'action',)
    class Media:
        js = ['js/music_song_extention.js']

    def get_model_perms(self, request):
        return {}
    


admin.site.register(MusicAlbum, MusicAlbumAdmin)
admin.site.register(Song, SongAdmin)

