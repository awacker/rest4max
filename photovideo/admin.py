from django.contrib import admin
from django import forms
from .models import *



class PhotoVideoAlbumForm(forms.ModelForm):  
    title_value = forms.CharField(label="Title", max_length=200, required=True)
    class Meta:
        fields = [
            'title_value',
            'photo',
            'rank_value',
            ]

        
class PhotoVideoAlbumAdmin(admin.ModelAdmin):
    form = PhotoVideoAlbumForm
    list_display = ('title', 'rank', 'action',)
    readonly_fields = ('rank_value',)
    ordering = ['rank_value']
    class Media:
        js = ['js/photo_album_extention.js']


class PhotoVideoForm(forms.ModelForm):
    IMAGE, VIDEO = 'image', 'video'
    TYPE_CHOICES = (
        (IMAGE, 'image'),
        (VIDEO, 'video'),
    )
    
    link = forms.CharField(label="YouTobe link", max_length=200, required=False)
    type_value = forms.ChoiceField(label="Type", choices=TYPE_CHOICES, widget=forms.RadioSelect)
    
    class Meta:
        fields = [
            'photovideo_album_value',
            'type_value',
            'image',
            'link',
            'thumbnail',
            ]
    
    def __init__(self, data=None, *args, **kwargs):
        super(PhotoVideoForm, self).__init__(data, *args, **kwargs)
        self.fields['type_value'].initial = self.IMAGE

        if data and data.get('type_value', None) == self.VIDEO:
            self.fields['link'].required = True
            self.fields['image'].required = False
        else:
            self.fields['link'].required = False
            self.fields['image'].required = True            
    


class PhotoVideoAdmin(admin.ModelAdmin):
    form = PhotoVideoForm
    list_display = ('photovideo', 'type', 'rank', 'action',)
    ordering = ['rank_value']
    
    class Media:
        js = ['js/photo_extention.js']
        
    def get_model_perms(self, request):
        return {}

admin.site.register(PhotoVideoAlbum, PhotoVideoAlbumAdmin)
admin.site.register(PhotoVideo, PhotoVideoAdmin)
