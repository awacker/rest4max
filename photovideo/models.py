from django.db import models
import os
from django.conf import settings
from django.dispatch import receiver
import shutil
from thumbs import ImageWithThumbsField, generate_thumb
import storages.backends.s3boto

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.db.models import Max
from django.db.models import F

photo_album_path_pref = 'photo/'

def PhotoVideoAlbumFile(instance, filename):
    r_id = instance.title_value.replace(' ', '_').replace('/', '-')
    url = "%salbums_title_photos/%s/%s" % (photo_album_path_pref, r_id, filename)
    return url

class PhotoVideoAlbum(models.Model):
    title_value = models.CharField(max_length=200, unique=True)
    photo = models.FileField(upload_to=PhotoVideoAlbumFile, blank=True, null=True) 
    rank_value = models.IntegerField(blank=True, null=True)
    
    def _title_value(self):
        return self.title_value
    title = property(_title_value)
    
    def _rank_value(self):
        return self.rank_value
    rank = property(_rank_value)
    
    def _action(self):
        return 'action!:%s!:%s!:%s;' % (self.id, self.rank_value, self.title_value)
    action = property(_action)
    
    
    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.id == None:
            v = PhotoVideoAlbum.objects.all().aggregate(Max('rank_value'))['rank_value__max']
            if v == None:
                v = '0'
            self.rank_value = int(v) + 1
        super(PhotoVideoAlbum, self).save(*args, **kwargs)
        
        

def PhotoVideoFile(instance, filename):

    r_id = instance.photovideo_album_value.id
    url = "%s%s/%s" % (photo_album_path_pref, r_id, filename)
    return url

def ThumbnailFile(instance, filename):

    r_id = instance.photovideo_album.id 
    url = "%s%s/%s" % (photo_album_path_pref, r_id, filename)
    return url

class PhotoVideo(models.Model):
    photovideo_album_value = models.ForeignKey(PhotoVideoAlbum)
    image = models.FileField(upload_to=PhotoVideoFile, blank=True, null=True)
    link = models.CharField(max_length=200, blank=True, null=True)
    thumbnail = models.FileField(upload_to=ThumbnailFile, blank=True, null=True)
    type_value = models.CharField(max_length=10, blank=True, null=True)
    rank_value = models.IntegerField(blank=True, null=True)

    def _rank_value(self):
        return self.rank_value
    rank = property(_rank_value)
    
    def _type(self):
        return self.type_value
    type = property(_type)
    
    def _action(self):
        return 'action!:%s!:%s!:%s;' % (self.id, self.rank_value, self.photovideo_album_value.id)
    action = property(_action)
    
    def _photovideo(self):
        if self.type == 'image':
            return self.image.name
        else:
            return self.link
    photovideo = property(_photovideo)
    
    def save(self, *args, **kwargs):
        if self.id == None:
            v = PhotoVideo.objects.all().filter(photovideo_album_value=self.photovideo_album_value.id).aggregate(Max('rank_value'))['rank_value__max']
            if v == None:
                v = '0'
                
#            print v, self.photovideo_album_value.id
            self.rank_value = int(v) + 1
            
        if self.type_value == 'image':
            self.link = ''
            split = str(self.image).decode('utf-8').rsplit('/', 1)
            if len(split) > 1:
                split = split[1].rsplit('.', 1)
            else:
                split = split[0].rsplit('.', 1)
            print split, self.thumbnail, '00'
            if split[1].lower() in ['jpg', 'jpeg', 'gif', 'png'] and str(self.thumbnail) == '':
                x = 150
                print split, self.thumbnail, '00'
                directory = self.photovideo_album_value.id 
                thumb_name = '%s%s/%s.%sx%s.%s' % (photo_album_path_pref, directory, split[0], x, x, split[1])
                thumb_content = generate_thumb(self.image.file, (x, x), split[1])
                default_storage.delete(thumb_name)
                thumb_name_ = default_storage.save(thumb_name, thumb_content)
                
                self.thumbnail = thumb_name_
                
        else:
            try:
                default_storage.delete(str(self.image))
            except:
                pass            

             
        super(PhotoVideo, self).save(*args, **kwargs)


@receiver(models.signals.post_delete, sender=PhotoVideoAlbum)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    PhotoVideoAlbum.objects.filter(rank_value__gt=instance.rank_value).update(rank_value=F('rank_value') - 1)
    try:
        default_storage.delete(str(instance.photo))
    except:
        pass
    
@receiver(models.signals.post_delete, sender=PhotoVideo)
def auto_delete_photovideo_file_on_delete(sender, instance, **kwargs):
    PhotoVideo.objects.filter(rank_value__gt=instance.rank_value, photovideo_album_value=instance.photovideo_album_value).update(rank_value=F('rank_value') - 1)
    try:
        default_storage.delete(str(instance.image))
        default_storage.delete(str(instance.thumbnail))
    except:
        pass     
    
    
