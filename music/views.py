from django.shortcuts import *
from rest_framework.response import Response
from django.conf import settings

from rest_framework import viewsets, routers, serializers

from music.models import *
from django.views.decorators.csrf import csrf_exempt
import traceback
import json


class MusicAlbumSerializer(serializers.ModelSerializer):
    photo1 = serializers.SerializerMethodField('_photo1')
    photo2 = serializers.SerializerMethodField('_photo2')
    photo3 = serializers.SerializerMethodField('_photo3')
    rank = serializers.SerializerMethodField('_rank')
    
    def _rank(self, obj):
        return obj.rank_value

    def _photo1(self, obj):
        if obj.photo1 != '':
            return default_storage.url(str(obj.photo1)).split('?')[0]
        else:
            return ''
    def _photo2(self, obj):
        if obj.photo2 != '':
            return default_storage.url(str(obj.photo2)).split('?')[0]
        else:
            return ''
    def _photo3(self, obj):
        if obj.photo3 != '':
            return default_storage.url(str(obj.photo3)).split('?')[0]
        else:
            return ''

    def _song_URL(self, obj):  
        return default_storage.url(str(obj.file_name)).split('?')[0]    
    
    class Meta:
        model = MusicAlbum
        fields = ('id', 'title', 'photo1', 'photo2', 'photo3', 'discription', 'rank',)

class MusicAlbumViewSet(viewsets.ModelViewSet):
    model = MusicAlbum
    
    def list(self, request): 
        queryset = MusicAlbum.objects.all().order_by('rank_value')
        serializer = MusicAlbumSerializer(queryset, many=True)
        return Response(serializer.data)

class SongSerializer(serializers.ModelSerializer):
    music_album = serializers.SerializerMethodField('_music_album')
    song_name = serializers.SerializerMethodField('_song_name')
    song_url = serializers.SerializerMethodField('_song_URL')
    rank = serializers.SerializerMethodField('_rank')
    
    def _rank(self, obj):
        return obj.rank_value

    def _music_album(self, obj):
        return obj.music_album_value.id
    
    def _song_name(self, obj):
        return obj.name

    def _song_URL(self, obj):      
        return default_storage.url(str(obj.file_name)).split('?')[0]
    
    class Meta:
        model = Song
        fields = ('id', 'music_album', 'song_name', 'song_url', 'lyrics', 'rank',)

    
class SongViewSet(viewsets.ModelViewSet):
    model = Song
    
    def list(self, request): 
#        print request.GET 
        if 'music_album' in request.GET:
            queryset = Song.objects.all().order_by('rank_value').filter(music_album_value=request.GET['music_album'])
        else:
            queryset = Song.objects.all().order_by('rank_value')
        serializer = SongSerializer(queryset, many=True)
        return Response(serializer.data)

#============================================


@csrf_exempt     
def music_album_up_rank(request): 
        
    try:
        print '=== music_album_up_rank === ', request.POST 
        
        temp_rank = int(request.POST['rank']) - 1
        if temp_rank > 0:
            MusicAlbum.objects.filter(rank_value=temp_rank).update(rank_value=-1)
            MusicAlbum.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            MusicAlbum.objects.filter(rank_value=-1).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    #    return HttpResponse('OK')
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        
    
@csrf_exempt     
def music_album_down_rank(request): 

    
    try:
        print '=== music_album_down_rank === ', request.POST   
        temp_rank = int(request.POST['rank']) + 1
        if 1 > 0:
            if MusicAlbum.objects.filter(rank_value=temp_rank).update(rank_value=-1) > 0:
                MusicAlbum.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            MusicAlbum.objects.filter(rank_value=-1).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        
@csrf_exempt   
def music_album_name(request):
    try:
        print '=== music_album_name === ', request.GET   
        id = request.GET['id']
        album_name = MusicAlbum.objects.get(id=id).title
        result = {'album_name':album_name}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        


@csrf_exempt     
def song_up_rank(request): 
        
    try:
        print '=== song_up_rank === ', request.POST 
        
        temp_rank = int(request.POST['rank']) - 1
        if temp_rank > 0:
            Song.objects.filter(rank_value=temp_rank, music_album_value=request.POST['album_id']).update(rank_value=-1)
            Song.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            Song.objects.filter(rank_value=-1, music_album_value=request.POST['album_id']).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    #    return HttpResponse('OK')
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
                
        
@csrf_exempt     
def song_down_rank(request): 

    try:
        print '=== song_down_rank === ', request.POST   
        temp_rank = int(request.POST['rank']) + 1
        if 1 > 0:
            if Song.objects.filter(rank_value=temp_rank, music_album_value=request.POST['album_id']).update(rank_value=-1) > 0:
                Song.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            Song.objects.filter(rank_value=-1, music_album_value=request.POST['album_id']).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        
