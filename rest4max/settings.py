"""
Django settings for rest4max project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import dj_database_url
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'fs9kxjji2i&!j0&kdsr3*x+^vfo32^f%20=8ukvkz))v9#(%2-'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'music',
    'photovideo',
    'websettings',
    'ckeditor',
    'django_wysiwyg',
    'storages',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'rest4max.urls'

WSGI_APPLICATION = 'rest4max.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DJANGO_WYSIWYG_FLAVOR = "ckeditor"


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'staticfiles'
STATICFILES_DIRS = (
                    os.path.join(BASE_DIR, 'static'),
                    )

if not os.path.exists(os.path.join(BASE_DIR, '.DS_Store')):
    DATABASES['default'] = dj_database_url.config()

PROVISION_DIR = BASE_DIR + '/provisioning_repository/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'



STORAGE_URL = 'http://awacker.herokuapp.com' + MEDIA_URL

REST_FRAMEWORK = {
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',
 
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

# REST_FRAMEWORK = {
# #     'DEFAULT_AUTHENTICATION_CLASSES': (
# #         'rest_framework.authentication.OAuth2Authentication',
# #     ),
#     'DEFAULT_PERMISSION_CLASSES': (
#         'rest_framework.permissions.IsAuthenticated',
#     )
# }



# Amazon S3 credentials
# AWS_ACCESS_KEY_ID       = os.environ['AWS_ACCESS_KEY_ID']
# AWS_SECRET_ACCESS_KEY   = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_ACCESS_KEY_ID = 'AKIAJPUB3U6D4GLBBOZA'
AWS_SECRET_ACCESS_KEY = '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'
# Amazon S3 URL
AWS_STORAGE_BUCKET_NAME = 'awacker'

AWS_ACCESS_KEY_ID = 'AKIAIQUEVS3OTUOXRXOQ'
AWS_SECRET_ACCESS_KEY = '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'
# Amazon S3 URL
AWS_STORAGE_BUCKET_NAME = 'gregghill'

S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
 
# Static files location
# STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
# STATIC_URL = S3_URL
 
# Default File storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
MEDIA_URL = S3_URL
 
# If you need to use Amazon S3 using http instead of https change this constant to False
AWS_S3_SECURE_URLS = False


CKEDITOR_UPLOAD_PATH = os.path.join(BASE_DIR, 'ckeditor')
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Full',
        'width': "100%",
        'height': "100px",
    },
}

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
#    BASE_DIR + '/templates/',
    BASE_DIR + '/admin/templates/',

)