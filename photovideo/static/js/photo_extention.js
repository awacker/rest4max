function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}


;(function($){ $(document).ready(function(){
	
	
	function GetAlbumName(id,mode){

	 	$.ajax({
	        url: "/photovideo/photovideo_album_name",
	        global: false,
	        type: 'GET',
	        dataType: "json",
	        data: {id: id},
	        success: function(data) { 
	        	var h = '<div class="breadcrumbs">'+
	        	'<a href="/admin/">Home</a> › '+
	        	'<a href="/admin/photovideo/">Photovideo</a> › '+
	        	'<a href="/admin/photovideo/photovideoalbum/">Photo video albums</a> › '		
	        	if (mode == 0) {
	        		//h = h + ;
	        		h = h + '<a href="/admin/photovideo/photovideo/?photovideo_album_value='+id+'">'+data['album_name']+'</a> › Photo video';
	        	} else {
	        		h = h + data['album_name'];
	        	}
	        	h = h + '</div>';
	        	$('.breadcrumbs').replaceWith(h);
	        } 
	 	});
	 	
	}

	var params = getSearchParameters();
	
	console.log(params);
	if (params['_changelist_filters']) {		
		var album_id = params['_changelist_filters'].split('%3D')[1];
		var i;
		var sel_list = $('select option').children().prevObject;
		for (i = 0; i < sel_list.length; i++) {
			if (sel_list[i]['value'] == album_id) {
				sel_list[i]['selected'] = true;
			}
		}
		$('.field-photovideo_album_value').attr('style', 'display: none;');
		
		console.log(album_id);
		GetAlbumName(album_id,0);
	} else {
		var album_id = params['photovideo_album_value'];
		console.log(album_id);
		GetAlbumName(album_id,1);
	}
	console.log(album_id);

	
	var r_list = $('td:contains("action")').text().split(';');
	var i;
	
	for (i = 0; i < r_list.length - 1; i++) {
		
		var photovideo_id = r_list[i].split('!:')[1]
		var photovideo_rank = r_list[i].split('!:')[2]
		var photovideo_album_id = r_list[i].split('!:')[3]
		
		var l = '<td class="action-tool">'+
//			'<a href="/admin/music/song/?music_album_value='+song_id+'" class="link">Go to Songs list</a>'+
			'<a id="up_'+photovideo_id+'_'+photovideo_rank+'_'+photovideo_album_id+'" href="#" class="link1">Rank UP</a>'+
			'<a id="down_'+photovideo_id+'_'+photovideo_rank+'_'+photovideo_album_id+'" href="#" class="link2">Rank DOWN</a>'+
			'</td>';
		$('td:contains("'+r_list[i]+'")').replaceWith(l);
		
	}
	$('.link1').bind('click', function(event){	
		var photovideo_id = event.target.id.split('_')[1];
		var photovideo_rank = event.target.id.split('_')[2];
		var photovideo_album_id = event.target.id.split('_')[3];
	 	$.ajax({
	        url: "/photovideo/photovideo_up_rank",
	        global: false,
	        type: 'POST',
	        dataType: "json",
	        data: {id: photovideo_id, rank: photovideo_rank, album_id: photovideo_album_id},
	        success: function(data) { 
	        	window.location.reload();
	        } 
	 	});
	});
	
	$('.link2').bind('click', function(event){
		var photovideo_id = event.target.id.split('_')[1];
		var photovideo_rank = event.target.id.split('_')[2];
		var photovideo_album_id = event.target.id.split('_')[3];
	 	$.ajax({
	        url: "/photovideo/photovideo_down_rank",
	        global: false,
	        type: 'POST',
	        dataType: "json",
	        data: {id: photovideo_id, rank: photovideo_rank, album_id: photovideo_album_id},
	        success: function(data) { 
	        	window.location.reload();
	        } 
	 	});
	});

   
  });
})(django.jQuery);

