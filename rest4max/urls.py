from django.conf.urls import *  # patterns, include, url

from django.contrib import admin
admin.autodiscover()
from django.conf import settings
from django.conf.urls.static import static


# from django.conf.urls.defaults import *

from music.views import *
from photovideo.views import *
from websettings.views import *

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'music_album', MusicAlbumViewSet)
router.register(r'song', SongViewSet)
# router.register(r'play_list', PlaylistViewSet)
# router.register(r'play_list_item', PlaylistItemViewSet)
router.register(r'photo_video', PhotoVideoViewSet)
router.register(r'photo-video-album', PhotoVideoAlbumViewSet)
router.register(r'web-settings', WebSettingsViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rest4max.views.home', name='home'),
    url(r'^photovideo/', include('photovideo.urls'), name='photovideo'),
    url(r'^music/', include('music.urls'), name='music'),
    url(r'^ckeditor/', include('ckeditor.urls')),
#    url(r'^ckeditor/', include('mystuff.ckeditor_urls')),
    url(r'^rest/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
