from django.db import models
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.dispatch import receiver

attach_path_pref = 'web_settings_attachment/'

def AttacheFile(instance, filename):
    url = "%s%s" % (attach_path_pref, filename)
    return url

class WebSettings(models.Model):
    name = models.CharField(max_length=100, unique=True)
    type = models.CharField(max_length=10)
    string = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    attachment = models.FileField(upload_to=AttacheFile, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "WebSettings"

    def save(self, *args, **kwargs):
        if type == 'string':
            self.text = ''
            self.attachment = ''
        elif type == 'text':
            self.string = ''
            self.attachment = ''
        if type == 'attachment':
            self.text = ''
            self.string = ''
             
        super(WebSettings, self).save(*args, **kwargs)
        
@receiver(models.signals.post_delete, sender=WebSettings)
def auto_delete_photovideo_file_on_delete(sender, instance, **kwargs):
    try:
        default_storage.delete(str(instance.attachment))
    except:
        pass     
