from django.contrib import admin
from django import forms
from .models import *
# from ckeditor.widgets import CKEditorWidget


class WebSettingsForm(forms.ModelForm):
    STRING, TEXT, ATTACHMENT, = 'string', 'text', 'attachment'
    TYPE_CHOICES = (
        (STRING, 'String'),
        (TEXT, 'Text'),
        (ATTACHMENT, 'Attachment File'),
    )
    
    name = forms.CharField(label="Variable name", max_length=100, required=True)
    type = forms.ChoiceField(choices=TYPE_CHOICES, widget=forms.RadioSelect)
#    text = forms.CharField(widget=CKEditorWidget())
    
    class Meta:
        fields = [
            'name',
            'type',
            'string',
            'text',
            'attachment',
            ]

    def __init__(self, data=None, *args, **kwargs):
        super(WebSettingsForm, self).__init__(data, *args, **kwargs)
        self.fields['type'].initial = self.STRING

        if data and data.get('type', None) == self.STRING:
            self.fields['string'].required = True
            self.fields['text'].required = False
            self.fields['attachment'].required = False
        elif data and data.get('type', None) == self.TEXT:
            self.fields['string'].required = False
            self.fields['text'].required = True
            self.fields['attachment'].required = False      
        elif data and data.get('type', None) == self.ATTACHMENT:
            self.fields['string'].required = False
            self.fields['text'].required = False
            self.fields['attachment'].required = True      

class WebSettingsAdmin(admin.ModelAdmin):
    form = WebSettingsForm
    list_display = ('name', 'type',)


admin.site.register(WebSettings, WebSettingsAdmin)
