from django.shortcuts import render
from django.conf import settings
from rest_framework.response import Response
from rest_framework import viewsets, routers, serializers

from websettings.models import *

class WebSettingsSerializer(serializers.ModelSerializer):
    var_value = serializers.SerializerMethodField('_var_value')
    def _var_value(self, obj):
        if obj.type == 'string':
           return  obj.string
        elif obj.type == 'text':
           return  obj.text
        elif obj.type == 'attachment':    
            return default_storage.url(str(obj.attachment)).split('?')[0]

    class Meta:
        model = WebSettings
        fields = ('name', 'type', 'var_value',)


class WebSettingsViewSet(viewsets.ModelViewSet):
    model = WebSettings
    
    def list(self, request): 
        if 'name' in request.GET:
            queryset = WebSettings.objects.all().filter(name=request.GET['name'])
        else:      
            queryset = WebSettings.objects.all()

        serializer = WebSettingsSerializer(queryset, many=True)
        return Response(serializer.data)
 



