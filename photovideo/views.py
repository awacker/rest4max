from django.shortcuts import *
from django.conf import settings
from rest_framework.response import Response
from rest_framework import viewsets, routers, serializers

from photovideo.models import *
from django.views.decorators.csrf import csrf_exempt
import traceback
import json

class PhotoVideoSerializer(serializers.ModelSerializer):
    rank = serializers.SerializerMethodField('_rank')
    type = serializers.SerializerMethodField('_type')
    image = serializers.SerializerMethodField('_image')
    photovideo_album = serializers.SerializerMethodField('_photovideo_album')
    thumbnail = serializers.SerializerMethodField('_thumbnail')
    def _image(self, obj):
        if obj.type == 'image':
            return default_storage.url(str(obj.image)).split('?')[0]
        else:
            return str(obj.link)

    def _thumbnail(self, obj):
        if obj.thumbnail != '':     
            return default_storage.url(str(obj.thumbnail)).split('?')[0]
        else:
            return ''
        
    def _photovideo_album(self, obj):
        return obj.photovideo_album_value.id

    def _rank(self, obj):
        return obj.rank_value
    
    def _type(self, obj):
        return obj.type_value

    class Meta:
        model = PhotoVideo
        fields = ('id', 'photovideo_album', 'rank', 'image', 'thumbnail', 'type',)



class PhotoVideoViewSet(viewsets.ModelViewSet):
    model = PhotoVideo
    
    def list(self, request): 
        if 'photovideo_album' in request.GET:
            queryset = PhotoVideo.objects.all().order_by('rank_value').filter(photovideo_album_value=request.GET['photovideo_album'])
        else:      
            queryset = PhotoVideo.objects.all().order_by('rank_value')
        serializer = PhotoVideoSerializer(queryset, many=True)
        return Response(serializer.data)
    
    
class PhotoVideoAlbumSerializer(serializers.ModelSerializer):
    rank = serializers.SerializerMethodField('_rank')
    title = serializers.SerializerMethodField('_title')
    photo = serializers.SerializerMethodField('_photo')
    def _photo(self, obj):
        if obj.photo != '':
            return default_storage.url(str(obj.photo)).split('?')[0]
        else:
            return ''

    def _title(self, obj):
        return obj.title_value

    def _rank(self, obj):
        return obj.rank_value
        
    class Meta:
        model = PhotoVideoAlbum
        fields = ('id', 'title', 'photo', 'rank',)


class PhotoVideoAlbumViewSet(viewsets.ModelViewSet):
    model = PhotoVideoAlbum
    
    def list(self, request): 
   
        queryset = PhotoVideoAlbum.objects.all().order_by('rank_value')
        serializer = PhotoVideoAlbumSerializer(queryset, many=True)
        return Response(serializer.data)
 
# ====================================

@csrf_exempt     
def photovideo_album_up_rank(request): 
        
    try:
        print '=== photovideo_album_up_rank === ', request.POST 
        
        temp_rank = int(request.POST['rank']) - 1
        if temp_rank > 0:
            PhotoVideoAlbum.objects.filter(rank_value=temp_rank).update(rank_value=-1)
            PhotoVideoAlbum.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            PhotoVideoAlbum.objects.filter(rank_value=-1).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        
    
@csrf_exempt     
def photovideo_album_down_rank(request): 

    
    try:
        print '=== photovideo_album_down_rank === ', request.POST   
        temp_rank = int(request.POST['rank']) + 1
        if 1 > 0:
            if PhotoVideoAlbum.objects.filter(rank_value=temp_rank).update(rank_value=-1) > 0:
                PhotoVideoAlbum.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            PhotoVideoAlbum.objects.filter(rank_value=-1).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')

@csrf_exempt     
def photovideo_album_name(request): 
    try:
        print '=== photovideo_album_name === ', request.GET   
        id = request.GET['id']
        album_name = PhotoVideoAlbum.objects.get(id=id).title_value
        result = {'album_name':album_name}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
        

@csrf_exempt     
def photovideo_up_rank(request): 
        
    try:
        print '=== photovideo_up_rank === ', request.POST 
        
        temp_rank = int(request.POST['rank']) - 1
        if temp_rank > 0:
            PhotoVideo.objects.filter(rank_value=temp_rank, photovideo_album_value=request.POST['album_id']).update(rank_value=-1)
            PhotoVideo.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            PhotoVideo.objects.filter(rank_value=-1, photovideo_album_value=request.POST['album_id']).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    #    return HttpResponse('OK')
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
                
        
@csrf_exempt     
def photovideo_down_rank(request): 

    try:
        print '=== photovideo_down_rank === ', request.POST   
        temp_rank = int(request.POST['rank']) + 1
        if 1 > 0:
            if PhotoVideo.objects.filter(rank_value=temp_rank, photovideo_album_value=request.POST['album_id']).update(rank_value=-1) > 0:
                PhotoVideo.objects.filter(id=request.POST['id']).update(rank_value=temp_rank)
            PhotoVideo.objects.filter(rank_value=-1, photovideo_album_value=request.POST['album_id']).update(rank_value=int(request.POST['rank']))
        result = {}
        return HttpResponse(json.dumps(result))
    except Exception as err:
        print('===========================================================================================')
        print("Type: {0} / Original massage: {1} /n {2}".format(type(err), err, traceback.format_exc(),))
        print('===========================================================================================')
       